# -*- coding: utf8 -*-

from vspk.vsdk.v3_1 import NUVSDSession as NUVSDSession_v3_1
from vspk.vsdk.v3_2 import NUVSDSession as NUVSDSession_v3_2
import os
import sys

def get_net_uuid(username, password, enterprise, api_url, api_version, l3domain_name, net_name):
    if api_version == "3.1":
        session = NUVSDSession_v3_1(
            username=username, password=password, enterprise=enterprise, api_url=api_url)
    elif api_version == "3.2":
        session = NUVSDSession_v3_2(
            username=username, password=password, enterprise=enterprise, api_url=api_url)
    else:
        return

    session.start()
    user = session.user
    domains=user.domains.get()
    for item in domains:
        if ( item.description == l3domain_name ):
            subnets = item.subnets.get()
            for nets in subnets:
                 if ( nets.name == net_name ):
                      return nets.id

if __name__ == "__main__":


    l3domain_name = sys.argv[1]
    net_name = sys.argv[2]
    
    api_url     = 'https://10.0.0.2:8443'
    api_version = '3.2'
    username    = 'csproot'
    password    = 'csproot'
    enterprise  = 'csp'
    
    print get_net_uuid(username=username, password=password, enterprise=enterprise,
                     api_url=api_url, api_version=api_version, l3domain_name=l3domain_name, net_name = net_name)

